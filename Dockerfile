FROM node:12 AS build

RUN sed -i -e 's/deb.debian.org/archive.debian.org/g' \
           -e 's|security.debian.org|archive.debian.org/|g' \
           -e '/stretch-updates/d' /etc/apt/sources.list

RUN apt-get update && apt-get install -y \
        git-core \
        build-essential \
        libsodium-dev \
        libboost-system-dev \
  && rm -rf /var/lib/apt/lists/*
RUN git clone --depth=1 https://gitlab.com/pandacoin/mining/cryptocurrency-pool-server.git /app && rm -rf /app/.git
COPY app /app
WORKDIR /app
RUN npm i
FROM node:12
COPY --from=build /etc/apt/sources.list /etc/apt/sources.list
RUN apt-get update && apt-get install -y \
        libsodium18 \
        libboost-system1.62.0 \
  && rm -rf /var/lib/apt/lists/*
COPY --from=build /app /app
WORKDIR /app
# weird
COPY app /app
CMD ["init.js"]
