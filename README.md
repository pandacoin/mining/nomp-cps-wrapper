#### Import wallet
<pre>
docker-compose up -d pandacoind # wait for it to finish syncing
docker-compose exec pandacoind bash -c 'pandacoin-cli -rpcuser=rpc -rpcpassword=rpc sethdseed true $HD_SEED'
docker-compose exec pandacoind bash -c 'pandacoin-cli -rpcuser=rpc -rpcpassword=rpc rescanblockchain'
docker-compose exec pandacoind bash -c 'pandacoin-cli -rpcuser=rpc -rpcpassword=rpc getbalance'
</pre>
