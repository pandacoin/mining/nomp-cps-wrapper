import requests
import time

def get_best_block_hash():
    rpc_url = "http://rpc:rpc@pandacoind:22444"
    payload = {
        "method": "getbestblockhash",
        "params": [],
        "jsonrpc": "2.0",
        "id": 1,
    }

    response = requests.post(rpc_url, json=payload)
    result = response.json()

    return result["result"]

def get_block_info(block_hash):
    rpc_url = "http://rpc:rpc@pandacoind:22444"
    payload = {
        "method": "getblock",
        "params": [block_hash, True],
        "jsonrpc": "2.0",
        "id": 1,
    }

    response = requests.post(rpc_url, json=payload)
    result = response.json()

    return result["result"]

def find_latest_pow_block():
    best_block_hash = get_best_block_hash()
    pos_block_count = 0

    while best_block_hash:
        block_info = get_block_info(best_block_hash)

        if "flags" in block_info and "proof-of-stake" not in block_info["flags"]:
            return block_info["time"], pos_block_count

        pos_block_count += 1
        best_block_hash = block_info.get("previousblockhash")

def main():
    last_status = False
    print("started monitor")

    while True:
        last_pow_block_time, pos_block_count = find_latest_pow_block()
        last_block_time = get_block_info(get_best_block_hash())["time"]

        if last_pow_block_time is None:
            print("last pow block time not found")
            break

        current_time = int(time.time())
        time_difference = current_time - last_pow_block_time

        if ((current_time - last_block_time) > 8000) or (time_difference > 8000 and pos_block_count > 4):
            if not last_status:
                last_status = True
                print(last_status)
        elif last_status:
            last_status = False
            print(last_status)
        time.sleep(60)

if __name__ == "__main__":
    main()
